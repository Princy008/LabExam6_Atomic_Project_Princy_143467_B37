<?php
namespace App\BookTitle;
use App\Model\Database as DB;
//use PDO;
//include("Database.php");
class BookTitle extends DB
{

    public $id = "";
    public $book_title = "";
    public $author_name = "";

    public function __construct()
    {
        parent::__construct();
    }

 public function  setData($data=NULL)
 {
     if (array_key_exists('id',$data))
     {

         $this->id=$data['id'];
     }
     if (array_key_exists('book_title',$data))
     {

         $this->book_title=$data['book_title'];
     }
     if (array_key_exists('author_name',$data))
     {

         $this->author_name=$data['author_name'];
     }
 }
    public function store()
    {
        $arrData=array($this->book_title,$this->author_name);


       $sql="insert into book_title( book_title,author_name)values (?,?)";
        echo $sql;

       $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);

    }


}